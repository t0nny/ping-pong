USE [master]
GO
/****** Object:  Database [bd-ping-pong]    Script Date: 08/03/2022 19:00:52 ******/
CREATE DATABASE [bd-ping-pong]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'bd-ping-pong', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\bd-ping-pong.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'bd-ping-pong_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\bd-ping-pong_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [bd-ping-pong] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [bd-ping-pong].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [bd-ping-pong] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [bd-ping-pong] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [bd-ping-pong] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [bd-ping-pong] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [bd-ping-pong] SET ARITHABORT OFF 
GO
ALTER DATABASE [bd-ping-pong] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [bd-ping-pong] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [bd-ping-pong] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [bd-ping-pong] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [bd-ping-pong] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [bd-ping-pong] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [bd-ping-pong] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [bd-ping-pong] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [bd-ping-pong] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [bd-ping-pong] SET  DISABLE_BROKER 
GO
ALTER DATABASE [bd-ping-pong] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [bd-ping-pong] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [bd-ping-pong] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [bd-ping-pong] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [bd-ping-pong] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [bd-ping-pong] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [bd-ping-pong] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [bd-ping-pong] SET RECOVERY FULL 
GO
ALTER DATABASE [bd-ping-pong] SET  MULTI_USER 
GO
ALTER DATABASE [bd-ping-pong] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [bd-ping-pong] SET DB_CHAINING OFF 
GO
ALTER DATABASE [bd-ping-pong] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [bd-ping-pong] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [bd-ping-pong] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [bd-ping-pong] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
EXEC sys.sp_db_vardecimal_storage_format N'bd-ping-pong', N'ON'
GO
ALTER DATABASE [bd-ping-pong] SET QUERY_STORE = OFF
GO
USE [bd-ping-pong]
GO
/****** Object:  Table [dbo].[persona]    Script Date: 08/03/2022 19:00:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[persona](
	[id_persona] [int] IDENTITY(1,1) NOT NULL,
	[cant_ganadas] [int] NOT NULL,
	[nombre] [varchar](50) NOT NULL,
	[fecha_partido] [date] NULL,
	[estado] [char](1) NULL,
	[puntos] [int] NOT NULL,
 CONSTRAINT [PK__persona__228148B082BD08AF] PRIMARY KEY CLUSTERED 
(
	[id_persona] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[persona] ON 

INSERT [dbo].[persona] ([id_persona], [cant_ganadas], [nombre], [fecha_partido], [estado], [puntos]) VALUES (1, 3, N'PETERS', CAST(N'2022-01-28' AS Date), N'A', 33)
INSERT [dbo].[persona] ([id_persona], [cant_ganadas], [nombre], [fecha_partido], [estado], [puntos]) VALUES (2, 2, N'GENESIS', CAST(N'2028-01-14' AS Date), N'A', 22)
INSERT [dbo].[persona] ([id_persona], [cant_ganadas], [nombre], [fecha_partido], [estado], [puntos]) VALUES (3, 1, N'LIA', NULL, N'A', 11)
INSERT [dbo].[persona] ([id_persona], [cant_ganadas], [nombre], [fecha_partido], [estado], [puntos]) VALUES (6, 3, N'XIMENA', NULL, N'A', 34)
INSERT [dbo].[persona] ([id_persona], [cant_ganadas], [nombre], [fecha_partido], [estado], [puntos]) VALUES (14, 2, N'JOEL', NULL, N'A', 24)
INSERT [dbo].[persona] ([id_persona], [cant_ganadas], [nombre], [fecha_partido], [estado], [puntos]) VALUES (15, 2, N'ELI', NULL, N'A', 26)
SET IDENTITY_INSERT [dbo].[persona] OFF
GO
USE [master]
GO
ALTER DATABASE [bd-ping-pong] SET  READ_WRITE 
GO
