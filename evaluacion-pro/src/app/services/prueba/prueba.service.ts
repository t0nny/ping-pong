import { APP_CONFIG } from '../../config/app-config';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class PruebaService {
    public URL_SERVER = APP_CONFIG.restUrl;
    public URL_ROOT_WS_PROY_LINEAS_INV= this.URL_SERVER + '/api/personas';

    constructor(private http: HttpClient) { }

  /**************************************************************************************************** */
  /***********************Servicios para /api/lineasInvestigacion************************************************ */
  
  buscarListaPersonas(): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_PROY_LINEAS_INV +'/buscarListaPersonas');
  }

  buscarListaJugador(): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_PROY_LINEAS_INV +'/buscarListaJugador');
  }

 /* lineasYSubLineasInv(idDepOferta: string): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_PROY_LINEAS_INV + '/lineasYSubLineasInv/' + idDepOferta);
  }*/

  grabarPersona(persona: any): Observable<any> {
    let result: Observable<Object>;
    result = this.http.post(this.URL_ROOT_WS_PROY_LINEAS_INV + '/grabarPersona', persona);
    return result;
  }

  buscarJugador(nombre:string): Observable<any> {
    return this.http.get(this.URL_ROOT_WS_PROY_LINEAS_INV +'/buscarJugador/'+nombre);
  }

   getReportPinPon(formato:string ): Observable<Blob> {
    return this.http.get(APP_CONFIG.restUrl +'/api/reports/getReportPinPon/'+formato, {responseType: 'blob'});
  }
}