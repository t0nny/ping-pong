import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { jqxGridModule } from 'jqwidgets-ng/jqxgrid';
import { jqxCheckBoxComponent, jqxCheckBoxModule } from 'jqwidgets-ng/jqxcheckbox';
import { PruebaService } from './services/prueba/prueba.service';
import { HttpClientModule } from '@angular/common/http';
import { jqxInputModule } from 'jqwidgets-ng/jqxinput';
import { ModalModule } from 'ngx-bootstrap/modal';
import { ModalComponentComponent } from './view/modal-view/modal-component.component/modal-component.componet';
import { NgxExtendedPdfViewerComponent, NgxExtendedPdfViewerModule } from 'ngx-extended-pdf-viewer';
import { jqxButtonComponent, jqxButtonModule } from 'jqwidgets-ng/jqxbuttons';


@NgModule({
  declarations: [
    AppComponent,
    ModalComponentComponent,
  ],
  imports: [
    ModalModule.forRoot(),
    BrowserModule,
    AppRoutingModule,
    jqxGridModule,
    HttpClientModule,
    jqxCheckBoxModule,
    jqxInputModule,
    NgxExtendedPdfViewerModule,
    jqxButtonModule,
  ],
  providers: [PruebaService],
  bootstrap: [AppComponent]
})
export class AppModule { }
