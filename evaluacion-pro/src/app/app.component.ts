import { Component, ViewChild } from '@angular/core';
import { jqxCheckBoxComponent } from 'jqwidgets-ng/jqxcheckbox';
import { jqxGridComponent } from 'jqwidgets-ng/jqxgrid';
import { jqxInputComponent } from 'jqwidgets-ng/jqxinput';
import { Subscription } from 'rxjs';
import { PruebaService } from './services/prueba/prueba.service';
import { ModalComponentComponent } from './view/modal-view/modal-component.component/modal-component.componet';

import { jqxButtonComponent } from "jqwidgets-ng/jqxbuttons";
import { NgxExtendedPdfViewerComponent } from "ngx-extended-pdf-viewer";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'evaluacion-pro';
  _sub: Subscription | undefined;
  acumB: any = 0;
  acumA: any = 0;
  esPar: boolean = false;
  acumServidor: any = 0;
  seguirJug: any = 0;
  inicioJuego: boolean = false;
  public mostrarTabla = false;
  puntosA: any;
  puntosB: any;

  //variables para pdf
  isCollapsedPdf: boolean = true;
  pdfSrc: string = '';
  filename: string = "";
  pdfToPreview: any;
  selectedFile: any={};
  selectFilePath: string="";
  selectFileb64: string="";
  isFileImage: boolean=false;
  isFileDocument: boolean=false;

  constructor(
    private periodoConvocatoria: PruebaService) {
  }

  @ViewChild('gridProyectoRegistrados') gridProyectoRegistrados: jqxGridComponent | undefined;
  @ViewChild('checkBoxJuagarA') checkBoxJuagarA: jqxCheckBoxComponent | undefined;
  @ViewChild('checkBoxJuagarB') checkBoxJuagarB: jqxCheckBoxComponent | undefined;
  @ViewChild('myinputNombreB') myinputNombreB: jqxInputComponent | undefined;;
  @ViewChild('myinputNombreA') myinputNombreA: jqxInputComponent | undefined;

  @ViewChild(ModalComponentComponent) myModal!: ModalComponentComponent; //modal para mensajes y advertencias
  //modal para mensajes y advertencias

  @ViewChild('pdfViewer') pdfViewer: NgxExtendedPdfViewerComponent| undefined;
  @ViewChild('buttonVolver')buttonVolver!: jqxButtonComponent;


  ngOnInit() {
    this.mostrarTabla = true
    this.puntosA= 0;
    this.puntosB= 0;
    //this.listarPersonas();
    //this.listarJugadores();
    this.verReportePinPon();
  }

  procesoSumaPuntoB(): void {
    if (this.inicioJuego) {
      this.acumB = this.acumB + 1
      this.acumServidor = this.acumServidor + 1
      this.selecionarGanador();
    } else {
      console.log('Juego no iniciado')
      this.myModal.alertMessage({ title: 'Ping Pong', msg: 'Juego no iniciado!' });
    }
  }

  procesoSumaPuntoA(): void {
    if (this.inicioJuego) {
      this.acumServidor = this.acumServidor + 1
      this.acumA = this.acumA + 1
      this.selecionarGanador();
    } else {
      this.myModal.alertMessage({ title: 'Ping Pong', msg: 'Juego no iniciado!' });
    }
  }

  selecionarGanador() {
    console.log("PUNTOS A", this.acumA);
    this.puntosA = this.acumA;
    console.log("PUNTOS B", this.acumB);
    this.puntosB = this.acumB;
    this.esPar = this.acumServidor % 2 == 0

    if (this.seguirJug > 10) {
      let ban = this.acumA - this.acumB;
      if (ban == 2) {
        //gana juagador A
        this.myModal.alertMessage({ title: 'Ping Pong', msg: 'Gana Juagador A!' });
        this.formarJson(this.myinputNombreA?.val(), this.acumA);
      }
      if (ban == -2) {
        //gana juagador B
        this.myModal.alertMessage({ title: 'Ping Pong', msg: 'Gana Juagador B!' });
        this.formarJson(this.myinputNombreB?.val(), this.acumB);
      }

    } else {
      if (this.acumA == 10 && this.acumB == 10) {
        //AUMENTAR DOS PUNTOS
        this.seguirJug = this.acumA + 2;

      } else {
        if (this.acumA > 10) {
          //gana juagador A
          this.myModal.alertMessage({ title: 'Ping Pong', msg: 'Gana Juagador A!' });
          this.formarJson(this.myinputNombreA?.val(), this.acumA);

        }
        if (this.acumB > 10) {
          //gana juagador B
          this.myModal.alertMessage({ title: 'Ping Pong', msg: 'Gana Juagador B!' });
          this.formarJson(this.myinputNombreB?.val(), this.acumB);
        }
      }
    }
    if (this.esPar) {
      this.checkBoxJuagarA?.checked(!this.checkBoxJuagarA?.checked());
      this.checkBoxJuagarB?.checked(!this.checkBoxJuagarB?.checked());

    }
  }

  formarJson(nombre: string, puntos: number): any {
    this._sub = this.periodoConvocatoria.buscarJugador(nombre).subscribe(data => {
      let datosPersona: any = {};
      if (data) {
        console.log('JUGADOR ENCONTRADO' + data);
        datosPersona.id = data.id;
        datosPersona.nombre = nombre;
        //consulta a ala bd
        datosPersona.cantGanadas = data.cantGanadas + 1;
        datosPersona.puntos = data.puntos + puntos;
        datosPersona.estado = 'A';
        this.guardar(datosPersona);
      } else {
        console.log('JUGADOR NO ENCONTRADO');
        //consulta a ala bd si jugador exsite
        datosPersona.id = null;
        datosPersona.nombre = nombre;
        //consulta a ala bd
        datosPersona.cantGanadas = 1;
        datosPersona.puntos = puntos;
        datosPersona.estado = 'A';
        this.guardar(datosPersona);
      }
    });
  }

  guardar(persona: any) {
    this._sub = this.periodoConvocatoria.grabarPersona(persona).subscribe(result => {
      //para actulizar grid del padre(evaluar_proyecto)
      console.log('Se ha guardado la Persona Correctamente')
      this.resetJuego();
      this.listarJugadores();
    }, error => console.error("Error al grabar Persona " + error));
  }

  resetJuego() {
    this.acumB = 0;
    this.acumA = 0;
    this.puntosA= 0;
    this.puntosB= 0;
    this.esPar = false;
    this.acumServidor = 0;
    this.seguirJug = 0;
    this.checkBoxJuagarA?.checked(false);
    this.checkBoxJuagarB?.checked(false);
    this.myinputNombreA?.val('');
    this.myinputNombreB?.val('');
    this.inicioJuego = false;
  }

  proceso(): void {
    if (!this.myinputNombreA?.val() && !this.myinputNombreB?.val()) {
      //this.myModal.alertMessage({ title: 'Nueva Versión', msg: 'Seleccione un archivo!' });
      //implementar un modal de mensajes 
      this.myModal.alertMessage({ title: 'Ping Pong', msg: 'Debe ingresar los nombres de los jugadores!' });
    } else {
      if (this.inicioJuego) {
        this.myModal.alertMessage({ title: 'Ping Pong', msg: 'El juego en proceso!' });
      } else {
        this.checkBoxJuagarA?.checked(true);
        //this.myModal.alertMessage({ title: 'Ping Pong', msg: 'El juego ha comenzado!' });
        console.log("El juego ha comenzado")
        this.mostrarTabla=true;
        this.inicioJuego = true;
      }
    }
  }

  listarPersonas() {
    this._sub = this.periodoConvocatoria.buscarListaPersonas().subscribe(data => {
      console.log(data);
    });
  }

  listarJugadores() {
    this._sub = this.periodoConvocatoria.buscarListaJugador().subscribe(data => {
      console.log(data);
      this.mostrarTabla=false;
      this.sourcePersonasRegistrados.localdata = data;
      this.dataAdapterPersonasRegistrados.dataBind();
    });
  }

  sourcePersonasRegistrados: any =
    {
      datatype: 'array',
      id: 'idPersona',
      datafields:
        [
          { name: 'idPersona', type: 'int' },
          { name: 'nombre', type: 'string' },
          { name: 'cantGanadas', type: 'int' },
          { name: 'puntos', type: 'int' },
          //{ name: 'fechaHasta', type: 'date', format: 'd' },
        ]
    };
  dataAdapterPersonasRegistrados: any = new jqx.dataAdapter(this.sourcePersonasRegistrados);

  columnsProyectos: any[] =
    [
      { text: 'Id Persona', datafield: 'idPersona', width: '2%', filtertype: 'none', hidden: true },
      { text: 'Juagador', datafield: 'nombre', width: '30%' },
      { text: 'Partidas Ganadas', datafield: 'cantGanadas', width: '40%', cellsalign: 'center', align: 'center' },
      { text: 'Total Puntos', datafield: 'puntos', width: '30%', cellsalign: 'center', align: 'center' },
      //{ text: 'Fecha Desde', datafield: 'fechaDesde', width: '10%', cellsformat: 'yyyy-MM-dd', cellsalign: 'center', center: 'center' },

    ];

    verReportePinPon() {
      let formato = "pdf";
      this._sub = this.periodoConvocatoria.getReportPinPon(formato).subscribe((blob: Blob) => {
        
        if (this.isCollapsedPdf != false) {
              this.habilitarPdf();
              console.log('entro seguimiento')
          } else {
              console.log('no entro actividad')
          }
          this.filename = this.nombreArchivo();
          const objectUrl = window.URL.createObjectURL(blob);
          const enlace = document.createElement('a');
          enlace.href = objectUrl;
          this.pdfSrc = "";
          this.pdfSrc = enlace.href;
          //this.pdfToPreview=objectUrl;
          this.nombreArchivoDescarga(this.filename);
      });
  }

    habilitarPdf() {
      this.buttonVolver.disabled(false);
      this.isCollapsedPdf = !this.isCollapsedPdf;
  }

  nombreArchivo(): string {
      this.filename = "";
      var fecha = new Date();
      this.filename = fecha.getHours() + "_" + fecha.getMinutes() + "_" + fecha.getDate() + "_" + fecha.getMonth() + "_" + fecha.getFullYear()
      return this.filename;
  }


  nombreArchivoDescarga(filenameForDownload: string) {
      if (NgxExtendedPdfViewerComponent.ngxExtendedPdfViewerInitialized) {
          (<any>window).PDFViewerApplication.appConfig.filenameForDownload = filenameForDownload;
          (<any>window).PDFViewerApplication.appConfig.showZoomButtons = true;
          (<any>window).PDFViewerApplication.appConfig.showSecondaryToolbarButton = true;
          (<any>window).PDFViewerApplication.appConfig.showFindButton = true;
          (<any>window).PDFViewerApplication.appConfig.showPrintButton = true;
          (<any>window).PDFViewerApplication.appConfig.showPagingButtons = true;
          (<any>window).PDFViewerApplication.appConfig.showZoomButtons = true;
          (<any>window).PDFViewerApplication.appConfig.showDownloadButton = true;
          (<any>window).PDFViewerApplication.appConfig.showSidebarButton = true;
      }
  }

  //evnto del boton volver
  controlIsCollapsed() {
      this.pdfSrc = "";
      this.isCollapsedPdf = !this.isCollapsedPdf;
      this.buttonVolver.disabled(true);
  }

}
