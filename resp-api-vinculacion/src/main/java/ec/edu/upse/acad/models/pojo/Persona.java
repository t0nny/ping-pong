package ec.edu.upse.acad.models.pojo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Version;

import lombok.Getter;
import lombok.Setter;

//se pudiera llamar entity en ves de pojo
@Entity
@Table(schema = "dbo", name = "persona")
public class Persona {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_persona")
	@Getter	@Setter	private Integer id;
	
	@Column(name = "cant_ganadas")
	@Getter	@Setter	private Integer cantGanadas;

	@Getter	@Setter	private String nombre;

	@Getter	@Setter	private Integer puntos;
	
	@Getter	@Setter	private String estado;
	
	@PrePersist
	void preInsert() {
		if (this.estado == null)
			this.estado = "A";
	}


	

}
