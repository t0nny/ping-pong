package ec.edu.upse.acad.ws;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.upse.acad.models.pojo.Persona;
import ec.edu.upse.acad.models.repository.PersonaRepository;
import ec.edu.upse.acad.models.services.ILineaInvestigacionService;
import ec.edu.upse.acad.models.services.PersonaService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/personas")
public class PersonasController {
	@Autowired
	private PersonaService personaService;
	@Autowired
	private PersonaRepository personaRepository;
	
	@Autowired private ILineaInvestigacionService lineaInvestigacionService;
	
	@GetMapping("/buscarListaPersonas")
	public List<Persona> listaClientes(){
		return lineaInvestigacionService.findAll();
	}
	
	@RequestMapping(value = "/grabarPersona", method = RequestMethod.POST)
	public ResponseEntity<?> grabarPersona( // definimos
																											// que tenga
																											// autorizacion
			@RequestBody Persona persona) {
		try {
			// System.out.println(proyecto.toString());
			personaService.grabarPersona(persona);

		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(" ya" + e.getMessage() + "este " + e.getCause());
			//extractStackTrace(e, logger);
			// return ResponseEntity.status((HttpStatus.NOT_FOUND)).body(null);
		}

		return ResponseEntity.ok().build();
	}
	
	// buscar jugador por nombre
			@RequestMapping(value = "/buscarJugador/{nombre}", method = RequestMethod.GET)
			public ResponseEntity<?> buscarJugador(
					@PathVariable("nombre") String nombre) {
				return ResponseEntity.ok(personaRepository.buscarJugador(nombre));
			}

}
