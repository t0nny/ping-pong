package ec.edu.upse.acad;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RespApiVinculacionApplication {

	public static void main(String[] args) {
		SpringApplication.run(RespApiVinculacionApplication.class, args);
	}

}
