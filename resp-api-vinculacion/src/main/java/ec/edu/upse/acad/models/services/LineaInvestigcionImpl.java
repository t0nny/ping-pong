package ec.edu.upse.acad.models.services;

import java.util.List;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.upse.acad.models.pojo.Persona;
import ec.edu.upse.acad.models.repository.PersonaRepository;


@Service
public class LineaInvestigcionImpl implements ILineaInvestigacionService {
	
	@Autowired private PersonaRepository lineaInvestigacionRepository;
	
	@Transactional(readOnly = true)
	public List<Persona> findAll() {
		return (List<Persona>) lineaInvestigacionRepository.findAll();
	}

}
