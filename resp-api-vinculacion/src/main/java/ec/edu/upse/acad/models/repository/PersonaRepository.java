package ec.edu.upse.acad.models.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

//EL repositorio se podria remmplazar por el dao


import ec.edu.upse.acad.models.pojo.Persona;

public interface PersonaRepository extends JpaRepository<Persona, Integer> {
	
	@Transactional
	@Query(value = "SELECT p FROM Persona p " + " WHERE p.nombre=(?1) ")
	Persona buscarJugador(String nombre);


}
