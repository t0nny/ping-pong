package ec.edu.upse.acad.models.services;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.net.MalformedURLException;
import java.nio.file.Paths;
import java.sql.Connection;
import java.util.Map;
import java.util.UUID;

import javax.servlet.ServletContext;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ResourceUtils;

import lombok.Getter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.XlsxReportConfiguration;

@Service
@Transactional
public class FileService {
	
	@Autowired private ServletContext context; //local
	
	@Autowired private DataSource dataSource;
	//private static final String PATH_REPORT ="tmp"+File.separator+"vinculacion"+File.separator+"proyectos"+File.separator;
	 //private static final String PATH_REPORTES = File.separator+"src"+File.separator+"main"+File.separator+"resources"+File.separator+"rep";//LOCAL
	// private static final String PATH_TEMPORAL = File.separator+"src"+File.separator+"main"+File.separator+"resources"+File.separator+"tmp";//LOCAL
	
	 private static final String PATH_REPORTES = "rep";//server
	private static final String PATH_TEMPORAL = "tmp";//server
	 
	@Getter private String imagen="logoUpse.png";
	File file =new File(imagen);//local
	String ruta =file.getAbsoluteFile().getParent();//local
	//String ruta =this.getClass().getResource("/").getPath();//server
		
	 
	 private String getRealPath(String resource) {
	    	return context.getRealPath("") + File.separator + resource; //todo este metodo local
	    }
	
		
	 /*public FileVinculacionService() throws Exception {
	        
	       
			this.fileStorageLocationVinculacion = Paths.get(PATH_REPORT) //creamos la carpeta
	                .toAbsolutePath().normalize();
	        /******************************************************************/
/*
	        try {
	            Files.createDirectories(this.fileStorageLocationVinculacion);
	        } catch (Exception ex) {
	            throw new Exception("No se pudo crear el directorio para almacenar los archivos.", ex);
	        }
	 }*/

	/*****************hecho por Peters*********************************/
    public Resource cargarFileDelServer(String fileName) throws Exception {
        try {
           // Path filePath = this.fileStorageLocationVinculacion.resolve(fileName).normalize();
            Resource resource = new UrlResource("file:"+ruta+PATH_REPORTES+ fileName);
            //Path path = new File(getClass()
        	//		.getResource("/tmp")
        	//		.getFile()).toPath();
            if(resource.exists()) {

                return resource;
            } else {
                throw new Exception("Archivo no encontrado1 " + "file:"+ruta+PATH_REPORTES+fileName);
            }
        } catch (MalformedURLException ex) {
            throw new Exception("Archivo no encontrado " +"file:"+ruta+PATH_REPORTES+ fileName, ex);
        }
    }
    /******************************************************************/
    public static String getResourceBasePath() {
        // Obtener el directorio
        File path = null;
        try {
            path = new File(ResourceUtils.getURL("classpath:").getPath());
        } catch (FileNotFoundException e) {
            // nothing to do
        }
        if (path == null || !path.exists()) {
            path = new File("");
        }

        String pathStr = path.getAbsolutePath();
        // Si se está ejecutando en eclipse, está al mismo nivel que el directorio de destino. Si se implementa en el servidor, está al mismo nivel que el paquete jar por defecto
        pathStr = pathStr.replace(File.separator+"target"+File.separator+"classes", "");

        return pathStr;
    }
    
    /****
	 * SERVICIO QUE PERMITE OBTENER EL REPORTE PASANDO POR PARAMETROS LA CARPETA DONDE SE ENCUENTRE ALOJADO EL JASPERTREPORT
	 * @param carpeta
	 * @param fileName
	 * @param parametros
	 * @param formato
	 * @return
	 * @throws Exception
	 */
	public Resource generaReporteParametrosFormatoCarpeta(String carpeta,String fileName, Map<String, Object> parametros ,String formato) throws Exception {
        try {
        
        	//File file =new File(fileName);
        	//String ruta =file.getAbsoluteFile().getParent();//local
        	
        	System.out.println("rutaService "+ ruta+PATH_REPORTES+File.separator+ carpeta);
        	System.out.println("rutaService1 "+ ruta);
        	System.out.println("rutaService2 "+ PATH_REPORTES);
        	System.out.println("rutaService3 "+ carpeta);
        	File fileReporte = new File(ruta+File.separator+ carpeta);
        	//CREA LA CARPETA SI NO EXISTE
    		if(!fileReporte.exists()) {
    			fileReporte.mkdir();
    		}		
        	String fileReport=fileReporte+File.separator + fileName;
        	System.out.println("ruta"+fileReport);
        //	String fileReport = getRealPath(PATH_REPORTES) + File.separator+ carpeta+File.separator + fileName;
        	String filePath="";
        	if(formato.equals("xlsx")) {
        		// filePath = getRealPath(PATH_TEMPORAL) + File.separator+ carpeta +File.separator+ UUID.randomUUID() + ".xlsx";
        		File filePathh = new File(ruta+PATH_TEMPORAL+File.separator+ carpeta);
        		if(!filePathh.exists()) {
        			filePathh.mkdir();
        		}	
            		 
        		filePath = filePathh+File.separator +  UUID.randomUUID()  + ".xlsx";
        		
        		JasperPrint jasperPrint;
    			jasperPrint = JasperFillManager.fillReport(fileReport.toString(), parametros, dataSource.getConnection());
    	        JRXlsxExporter exporterXls = new JRXlsxExporter();
    	        exporterXls.setExporterInput(new SimpleExporterInput(jasperPrint));
    	        exporterXls.setExporterOutput(new SimpleOutputStreamExporterOutput(filePath));
    	        XlsxReportConfiguration configuration = null ;
    			exporterXls.setConfiguration(configuration);;
    	        exporterXls.exportReport();
        	}else if (formato.equals("pdf")) {
        	  	 //filePath = getRealPath(PATH_TEMPORAL) + File.separator+ carpeta +File.separator+ UUID.randomUUID()  + ".pdf";
        		File filePathh = new File(ruta+File.separator+ carpeta);//server
        		if(!filePathh.exists()) {
        			filePathh.mkdir();
        		}	
            		 
        		filePath = filePathh+File.separator +  UUID.randomUUID()  + ".pdf";
        		byte[] b = null;
    			//System.out.println(parametros);
    		    
        		Connection conection =dataSource.getConnection();
        		
        		try {
        			b = JasperRunManager.runReportToPdf(fileReport, parametros, conection);
        			FileOutputStream fos = new FileOutputStream(filePath);
        			 
        			conection.close();
        			fos.write(b);
        			fos.close();
        			fos.flush();
					
				} finally {
					conection.close();
					// TODO: handle finally clause
				}
        		
    			
        	}
  
            Resource resource = new UrlResource(Paths.get(filePath).toUri());
            if(resource.exists()) {
                return resource;
            } else {
                throw new Exception("Archivo no encontrado " + fileName);
            }
        } catch (MalformedURLException ex) {
            throw new Exception("Archivo no encontrado11 " + fileName, ex);
        }
    }
	
	public Resource generaReporteParametrosCarpeta(String carpeta,String fileName, Map<String, Object> parametros ) throws Exception {
        try {
        	//File file =new File(fileName);//local
        	//String ruta =file.getAbsoluteFile().getParent();//local
        	
        	String fileReport = ruta+PATH_REPORTES + File.separator + carpeta+File.separator+ fileName;
        	
        	File filePathh = new File(ruta+PATH_TEMPORAL+ File.separator + carpeta);
        	//CREA LA CARPETA SI NO EXISTE
    		if(!filePathh.exists()) {
    			filePathh.mkdir();
    		}		
        	String filePath=filePathh+File.separator +  UUID.randomUUID()+ ".pdf";
			byte[] b = null;
			//System.out.println(parametros);
			Connection conection =dataSource.getConnection();
    		
			try {
    			b = JasperRunManager.runReportToPdf(fileReport, parametros, conection);
    			FileOutputStream fos = new FileOutputStream(filePath);
    			 
    			conection.close();
    			fos.write(b);
    			fos.close();
    			fos.flush();
				
			} finally {
				conection.close();
				// TODO: handle finally clause
			}
			
            
            Resource resource = new UrlResource(Paths.get(filePath).toUri());
            if(resource.exists()) {
                return resource;
            } else {
                throw new Exception("Archivo no encontrado " + fileName);
            }
        } catch (MalformedURLException ex) {
            throw new Exception("Archivo no encontrado " + fileName, ex);
        }
    }
}
