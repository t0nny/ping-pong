package ec.edu.upse.acad.ws;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.upse.acad.models.services.FileService;
import lombok.Getter;


@RestController
@RequestMapping("/api/reports")
@CrossOrigin
@MultipartConfig
public class ReportController {
	//@Autowired private ReportService reportService;
	@Autowired private FileService reportService;
	
	//private static final String PATH_IMAGEN = File.separator+"src"+File.separator+"main"+File.separator+"resources"+File.separator+"img";//local
	private static final String PATH_REPORT = File.separator+"src"+File.separator+"main"+File.separator+"resources"+File.separator+"rep";//local
	private static final String PATH_IMAGEN = "img";//server
	//private static final String PATH_REPORT = "rep";//server
		
	@Getter private String imagen="logoUpse.png";
	//@Getter private String rutaImagenBanerLocal="";
	//@Getter private String imagen1="vinculacionLogo1.png";
	//@Getter private String rutaImagenBanerLocal1="";
	//@Getter private String rutaArchivoLocal="";	
	//@Getter private String carpetaVinculacion="vinculacion";
	
	File file =new File(imagen);//local
	String ruta =file.getAbsoluteFile().getParent();//local
	//String ruta =this.getClass().getResource("/").getPath();//server
	
	
		@RequestMapping(value="/getReportPinPon/{formato}", method=RequestMethod.GET)
		public ResponseEntity<?> getReportPinPon(
				@PathVariable("formato") String formato,
				HttpServletRequest request) throws Exception {
			Map<String, Object> parametros = new HashMap<String, Object>();
			
			//rutaImagenBanerLocal  = ruta+PATH_IMAGEN+File.separator+imagen;
			//rutaImagenBanerLocal1  = ruta+PATH_IMAGEN+File.separator+imagen1;
			//rutaArchivoLocal  = ruta+PATH_REPORT+File.separator+carpetaVinculacion+File.separator;
			//parametros.put("rutaImagen", rutaImagenBanerLocal);
			//parametros.put("rutaImagen1", rutaImagenBanerLocal1);
			parametros.put("rutaArchivo", ruta+PATH_REPORT);
			// Carga el archivo
			System.out.println("oa"+parametros);
			Resource resource = reportService.generaReporteParametrosFormatoCarpeta(PATH_REPORT,"rpPinPon.jasper",parametros,formato);
			// Determina el contenido
			String contentType = null;
			try {
				contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
			} catch (IOException ex) {
				ex.printStackTrace();
			}

			// Si no se determina el tipo, asume uno por defecto.
			if(contentType == null) {
				if(formato=="pdf") {
					contentType = "application/octet-stream";
				}else if(formato=="xlsx") {
					contentType ="application/vnd.ms-excel";
				}
			}

			return ResponseEntity.ok().contentType(MediaType.parseMediaType(contentType))
					.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"").body(resource);
		}		
		
		
		
}
