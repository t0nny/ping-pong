package ec.edu.upse.acad.models.services;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.upse.acad.models.pojo.Persona;
import ec.edu.upse.acad.models.repository.PersonaRepository;
@Service
@Transactional
public class PersonaService {
	@PersistenceContext
	private EntityManager em;
	@Autowired
	private PersonaRepository personaRepository;
	
	public void grabarPersona(Persona persona) {
		System.out.println(persona.getId());
		if (persona.getId() != null) {
			System.out.println("editar registro");
			editarPersona(persona);
		} else {
			System.out.println("ingresar registro");
			nuevoPersona(persona);
		}		
	}

	public void nuevoPersona(Persona persona) {
		//Integer idProyecto=null;
		personaRepository.save(persona);
		//idProyecto=proyectoRepository.saveAndFlush(proyecto).getId();
		em.getEntityManagerFactory().getCache().evict(Persona.class);
	}
	
	public void editarPersona(Persona persona) {
		try {
			Persona _persona =personaRepository.findById(persona.getId()).get();
			BeanUtils.copyProperties(persona, _persona);
			personaRepository.save(_persona);
			em.getEntityManagerFactory().getCache().evict(Persona.class);	
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("aqui" + e.getMessage());
			//extractStackTrace(e, logger);
		}
	}

}
